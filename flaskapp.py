#!/usr/bin/python3

from flask import Flask, render_template, request, redirect, session, flash, send_from_directory
from werkzeug.utils import secure_filename

import os

UPLOAD_FOLDER = '/home/ubuntu/uploads/'
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}

app = Flask(__name__)
app.secret_key = 'smcieurhtskdjchrfrlewskjegvhrtkjgshcfdjggmsdvf'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.add_url_rule(
    "/uploads/<name>", endpoint="download_file", build_only=True
)
users = {}

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/register', methods = ['POST', 'GET'])
def register():
    if (request.method == 'POST'):
        username = request.form.get('username')
        password = request.form.get('password')
        firstname = request.form.get('firstname')
        lastname = request.form.get('lastname')
        email = request.form.get('email')

        if username in users.keys():
            return '<h3>Username is taken!</h3>' + render_template('register.html')
        
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        
        file = request.files['file']
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(file_path)
        else:
            return redirect(request.url)
        
        user = {
                'username': username,
                'password': password,
                'firstname':firstname,
                'lastname':lastname,
                'email':email,
                'file': filename
                }
        
        users[username] = user
        session['user'] = user
        return redirect('/display')

    return render_template('register.html')


@app.route('/', methods = ['POST', 'GET'])
def root():
    return redirect('/login')


@app.route('/login', methods = ['POST', 'GET'])
def login():
    ret = ''
    if(request.method == 'POST'):
        username = request.form.get('username')
        password = request.form.get('password')
        user = users.get(username)
        if user and password == user['password']:
            session['user'] = user
            return redirect('/display')
        ret = '<h3>Wrong username or password!</h3>'

    return ret + render_template('login.html')


@app.route('/logout', methods = ['POST', 'GET'])
def logout():
    session.pop('user')
    return redirect('/login')


@app.route('/uploads/<name>')
def download_file(name):
    return send_from_directory(app.config["UPLOAD_FOLDER"], name)

@app.route('/display', methods = ['GET'])
def display():
    if(request.method == 'GET'):
        user = session.get('user')
        path = os.path.join(app.config['UPLOAD_FOLDER'], user['file'])
        with open(path, 'r') as fh:
            contents = fh.read()
        word_count = len(contents.replace('\n', ' ').split(' '))
        if user:
            return render_template('display.html', user=user, word_count=word_count)
    return render_template('login.html')


if __name__ == '__main__':
  app.run()
